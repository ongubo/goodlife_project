<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
		$this->call('UserTableSeeder');
		$this->command->info('users table seeded!');

	}

}


class UserTableSeeder extends Seeder{
    
		    public function run()
		    {
		        DB::table('users');
		        User::create(array(
					'firstname'     => 'Admin',
					'secondname' => 'Admin',
					'email'    => 'admin@mail.com',
					'phone'    => '0717123456',
					'password' => Hash::make('admin'),
				));
			}
    }