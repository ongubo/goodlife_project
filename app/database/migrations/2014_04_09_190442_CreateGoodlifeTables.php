<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoodlifeTables extends Migration {
public function up()
	{

		Schema::create('houses', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('photo_name',50);
			$table->dateTime('outDate');
			$table->string('type',50);
			$table->decimal('cost',10,2);
			$table->string('location',50);
			$table->string('description',1000);
			$table->tinyInteger('no_of_bedrooms')->unsigned();
			$table->timestamps();
			$table->softDeletes();
		});

		Schema::create('users', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('firstname',50);
			$table->string('secondname',50);
			$table->string('email',50);
			$table->string('phone',50);
			$table->string('password',256);
			$table->timestamps();
			$table->softDeletes();
		});

		Schema::create('photographs', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('name',50);
			$table->string('type',50);
			$table->string('size',50);
			$table->string('caption',256);
			$table->string('firstname',50);
			$table->timestamps();
			$table->softDeletes();
		});

		
		Schema::create('house_features', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('h_id');
			//$table->foreign('house_id')->references('id')->on('houses');
			$table->string('feature',50);
			$table->softDeletes();

		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('houses');
		Schema::drop('photographs');
		Schema::drop('users');
		Schema::drop('house_features');
	}


}
