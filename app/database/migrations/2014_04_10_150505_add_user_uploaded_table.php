<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserUploadedTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('user_uploads', function(Blueprint $table)
		{

			$table->engine = 'InnoDB';

			$table->increments('id');
			$table->string('ip_address',20);
			$table->string('phone_contact',20);
			$table->string('email',40);
			$table->string('photo_name',50);
			$table->dateTime('outDate');
			$table->string('type',50);
			$table->decimal('cost',10,2);
			$table->string('location',50);
			$table->string('description',1000);
			$table->tinyInteger('no_of_bedrooms')->unsigned();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('user_uploads');
	}

}
