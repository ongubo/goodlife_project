<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
///go to home page
Route::get('/', function()
{
	return View::make('home');
});


///go to login page
Route::get('login', function()
{
	return View::make('login');
});

////post login form and check for error
Route::post('login', array('uses' => 'HomeController@processLogin'));

///logout a user
Route::get('logout', array('uses' => 'HomeController@processLogout'));

////access houses
Route::resource('houses', 'HouseController');


