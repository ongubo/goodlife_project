<link rel="stylesheet" href="../../../public/styles/style.css" type="text/css" />
<link rel="stylesheet" href="../../../public/styles/bootstrap.css" type="text/css" />

<!doctype html>
<html>
<head>
	<title>New House</title>
	{{ HTML::style('styles/style.css') }}
	{{ HTML::style('styles/bootstrap.css') }}
    {{ HTML::script('scripts/javascript.js') }}
    {{ HTML::script('scripts/select2.min.js') }}

</head>
<body>
<div id="mainbar">
<a href="{{ URL::to('logout') }}">Logout</a>
	{{ Form::open(array('url' => 'houses')) }}

		<!-- if there are login errors, show them here -->
		<!-- <p>
			{{ $errors->first('email') }}
			{{ $errors->first('password') }}
		</div> -->
        
        <div id="main_left">
		<div class="form-group" >
			{{ Form::label('file', 'Select Image') }}<br/>
			{{ Form::file('file') }}
		</div>
		<div class="col-xs-6 col-md-3" style="margin:-60px 0 0 180px; position:absolute; height:200px">
		    <li class="thumbnail">
		      <img data-src="" src="" alt="...">
		    </li>
		</div><br/>
		<div class="form-group" >
			{{ Form::label('house_type', 'House Type') }}<br/>
			{{ Form::select('house_type', array('bungalow' => 'Bungalow', 
												'town_house' => 'Town House',
												'double_storey' => 'Double Storey',
												'duples' => 'Duplex'  ), 'bungalow') }}
		</div>

		<div class="form-group" >
			{{ Form::label('cost', 'Cost') }}<br/>
			{{ Form::text('cost', null, array('class' => 'form-control'))}}
		</div>
		<div class="form-group" >
			{{ Form::label('location', 'Location') }}<br/>
			{{ Form::text('location')}}
		</div>
		<div class="form-group" >
			{{ Form::label('description', 'Description') }}<br/>
			{{ Form::textarea('description',null, ['class' => 'form-control'])}}
		</div>
        </div>
        <div id="main_right">
		<div class="form-group" >
			{{ Form::label('no_of_bedrooms', 'Number of Bedrooms') }}<br/>
			{{ Form::text('no_of_bedrooms',null, array('class' => 'form-control'))}}
		</div>
		<div class="form-group" >
			{{ Form::checkbox('features[]', 'sq', null, ['class' => '']) }}
			{{ Form::label('sq', 'Has an SQ') }}<br/>

			{{ Form::checkbox('features[]', 'pool')}}
			{{ Form::label('pool', 'Has a Pool') }}<br/>

			{{ Form::checkbox('features[]', 'internet')}}
			{{ Form::label('internet', 'Has Internet ') }}<br/>

			{{ Form::checkbox('features[]', 'generator')}}
			{{ Form::label('generator', 'Has Generator') }}<br/>

			{{ Form::checkbox('features[]', 'garden')}}
			{{ Form::label('garden', 'Has Garden') }}<br/>

			{{ Form::checkbox('features[]', 'furnished')}}
			{{ Form::label('furnished', 'Is Furnished') }}<br/>

			{{ Form::checkbox('features[]', 'gym')}}
			{{ Form::label('gym', 'Has A gym') }}<br/>
		</div>
		</div>
        <div style="clear:both"></div>
		<div class="form-group" >{{ Form::submit('Upload Property',array('class' => 'btn btn-success')) }}</div>
	{{ Form::close() }}
</div>
</body>
</html>
