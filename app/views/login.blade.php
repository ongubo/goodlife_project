<!-- app/views/login.blade.php -->
<?php
echo HTML::style('styles/style.css');

?>
<!doctype html>
<html>
<head>
	<title>Login</title>
	<link href="../../public/styles/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="../../public/styles/style.css" rel="stylesheet" type="text/css">
    {{HTML::style('styles/style.css')}}
    {{HTML::style('styles/bootstrap.css')}}
</head>
<body>
<div id="login">
        <div class="login_head">
        	<h3>Login</h3>
        </div>
	{{ Form::open(array('url' => 'login')) }}
		<!-- if there are login errors, show them here -->
		<p>
			{{ $errors->first('email') }}
			{{ $errors->first('password') }}
		</p>

		<p>
			{{ Form::label('email', '* Email Address ') }}
			{{ Form::text('email', Input::old('email'), array('placeholder' => 'mymail@mail.com','class' => 'form-control')) }}
		</p>

		<p>
			{{ Form::label('password', '* Password ') }}
			{{ Form::password('password', array('class'=>'form-control')) }}
		</p>

		<p>{{ Form::submit('Login',array('class' => 'btn btn-success')) }}</p>
	{{ Form::close() }}
</div>
</body>
</html>
