<?php

class HouseController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	 public function __construct()
    {
         $this->beforeFilter('auth');

         $this->beforeFilter('csrf', array('on' => 'post'));

        // $this->afterFilter('log', array('only' => array('fooAction', 'barAction')));
    }


	public function index()
	{
		//
		return View::make('admin.dashboard');
		//return View::make('login');
		//$this->loginForm();

	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		return View::make('admin.new');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$house_type = Input::get('house_type');
		$cost = Input::get('cost');
		$location = Input::get('location');
		$description = Input::get('description');
		$no_of_bedrooms = Input::get('no_of_bedrooms');
		$features = Input::get('features');

		if (Input::hasFile('file'))
		{
		    //
		}
		echo json_encode(Input::all());


	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}