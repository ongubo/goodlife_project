// JavaScript Document

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////manage select inputs that use select 2 library
      $(function(){
        function formatResult(item) {
          if(!item.id) {
            return item.text;
          }
          return '<i>' + item.text + '</i>';
        }

        function formatSelection(item) {
          return '<b>' + item.text + '</b>';
        }

        $("#house_type").select2({
          formatResult: formatResult,
          formatSelection: formatSelection
        });
      });